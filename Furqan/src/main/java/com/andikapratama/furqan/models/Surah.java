package com.andikapratama.furqan.models;

/**
 * Created by andikapratama on 9/6/13.
 */
public class Surah {
    final int no;
    final String name;
    final String translationName;
    final String arabicName;
    final String type;
    final int verseCount;

    public Surah(int no, String name, String translationName, String arabicName, String type, int verseCount) {
        this.no = no;
        this.name = name;
        this.translationName = translationName;
        this.arabicName = arabicName;
        this.type = type;
        this.verseCount = verseCount;
    }

    public String getTranslationName() {
        return translationName;
    }

    public int getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    public int getVerseCount() {
        return verseCount;
    }

    public String getArabicName() {
        return arabicName;
    }

    public String getType() {
        return type;
    }
}
