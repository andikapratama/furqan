package com.andikapratama.furqan.models;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by andikapratama on 9/6/13.
 */
public class Verse {
    final Map<Translation,String> translations;
    final String arabicText;
    final int number;
    final int surahNo;

    public Verse(int surahNo, int no,String arabicText) {
        this.number = no;
        this.surahNo = surahNo;
        translations = Maps.newLinkedHashMap();
        this.arabicText = arabicText;
    }

    public int getSurahNo() {
        return surahNo;
    }

    public Map<Translation, String> getTranslations() {
        return translations;
    }

    public int getNumber() {
        return number;
    }

    public String getArabicText() {
        return arabicText;
    }
}
