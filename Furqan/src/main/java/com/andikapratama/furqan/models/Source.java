package com.andikapratama.furqan.models;

import java.util.Date;

/**
 * Created by andikapratama on 30/7/13.
 */
public class Source implements Comparable<Source> {
    public final int id;
    public final String type;
    public final String name;
    public final String author;
    public final String downloadLink;
    public final String updateLink;
    public final Date lastModifiedDate;
    public final String language;
    public final String providerName;
    public String status;
    protected int order;

    public Source(int id, String type, String name, String author, String downloadLink, String updateLink, Date lastModifiedDate, String language, String providerName, String status) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.author = author;
        this.downloadLink = downloadLink;
        this.updateLink = updateLink;
        this.lastModifiedDate = lastModifiedDate;
        this.language = language;
        this.providerName = providerName;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Source)) return false;

        Source source = (Source) o;

        if (id != source.id) return false;

        return true;
    }

    @Override
    public int compareTo(Source source) {
        int a = this.getOrder();
        int b = source.getOrder();
        return a > b ? +1 : a < b ? -1 : 0;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
