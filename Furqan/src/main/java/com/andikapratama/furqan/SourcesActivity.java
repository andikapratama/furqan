package com.andikapratama.furqan;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.andikapratama.furqan.models.Source;
import com.andikapratama.furqan.views.dragsortlistview.DragSortListView;

import java.io.File;
import java.util.List;

/**
 * Created by andikapratama on 23/7/13.
 */
public class SourcesActivity extends ListActivity {

    private ListAdapter adapter;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sources_menu, menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sources_activity);

        List<Source> arrayList = FurqanApp.getInstance().getTranslationsService().getAvailableSources();

        adapter = new SourceListAdapter(this,arrayList);


        setListAdapter(adapter);

        DragSortListView list = getListView();
        SourceSectionController controller = new SourceSectionController(list,(SourceListAdapter) adapter);
        list.setDropListener((SourceListAdapter) adapter);
        list.setFloatViewManager(controller);
        list.setOnTouchListener(controller);
    }

    @Override
    public DragSortListView getListView() {
        return (DragSortListView) super.getListView();
    }

    @Override
    protected void onStop() {
        FurqanApp.getSettings().refreshTranslations();
        super.onStop();
    }

    private void downloadAndEnable(final Source source)
    {
        final DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        Request request = new Request(
                Uri.parse(source.downloadLink));
        request.setTitle("Furqan - " + source.name);

        final long enqueue = dm.enqueue(request);

        final BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c
                                .getInt(columnIndex)) {

                            String uriString = c
                                    .getString(c
                                            .getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));

                            File outputfile = new File(uriString);
                            FurqanApp.getTranslationsService().saveTanzilTranslationResourceAndEnableIt(source.id, outputfile);
                            outputfile.delete();
                            Toast.makeText(FurqanApp.getInstance(),source.name + " successfully installed.",Toast.LENGTH_SHORT).show();
                            FurqanApp.getInstance().unregisterReceiver(this);
                        }
                    }
                }
            }
        };

        FurqanApp.getInstance().registerReceiver(receiver, new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        Toast.makeText(this,"Download of "+ source.name + " started.",Toast.LENGTH_SHORT).show();


        return;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final Source source = (Source)adapter.getItem(position);
        if(source.status.equals("notdownloaded"))
        {
            new AlertDialog.Builder(this)
                    .setTitle("Add translation")
                    .setMessage("Download the source and enable it?")
                    .setNegativeButton("Cancel", null)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            downloadAndEnable(source);
                        }
                    })
                    .show();
        }

    }

}
