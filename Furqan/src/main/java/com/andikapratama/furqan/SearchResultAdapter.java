package com.andikapratama.furqan;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andikapratama.furqan.models.SearchResult;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andikapratama on 2/10/13.
 */
public class SearchResultAdapter extends BaseAdapter {


    List<SearchResult> results;
    Activity context;
    String query;
    Pattern pattern;

    public SearchResultAdapter(Activity context,List<SearchResult> results,String query)
    {
        this.context = context;
        this.results = results;
        this.query = query;
        pattern = Pattern.compile("(?i)"+query);
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public SearchResult getItem(int i) {
        return results.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    static class ViewHolder {
        public TextView surahTitle;
        public TextView verseTitle;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.search_row,null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.surahTitle = (TextView) rowView.findViewById(R.id.surahTextView);
            viewHolder.verseTitle = (TextView) rowView.findViewById(R.id.verseTextView);
            rowView.setTag(viewHolder);
        }

        SearchResult item = getItem(i);

        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.surahTitle.setText(FurqanApp.getTranslationsService().getAllSurah().get(item.getSurahNo()).getName() + " " + String.valueOf(item.getNumber()) + " - " + item.getTranslationName());
        String text = item.getText();
        Matcher matcher = pattern.matcher(text);
        List<String> replacement = Lists.newArrayList();

        // Check all occurance
        while (matcher.find()) {
            String match = text.substring(matcher.start(),matcher.end());
            replacement.add(match);
        }

        for (String replace : replacement)
        {
            text = text.replace(replace,"<b>"+replace+"</b>");
        }

        holder.verseTitle.setText(Html.fromHtml(text));
        return rowView;
    }
}
