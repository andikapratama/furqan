package com.andikapratama.furqan;

import android.app.Application;

import com.andikapratama.furqan.repository.FurqanDatabase;
import com.andikapratama.furqan.repository.TranslationsRepository;
import com.andikapratama.furqan.services.TranslationsService;

/**
 * Created by andikapratama on 13/6/13.
 */
public class FurqanApp extends Application
{
    private static FurqanApp sFurqanApp;
    private TranslationsRepository sTranslationsRepository;
    private TranslationsService sTranslationsService;
    private FurqanDatabase sFurqanDatabase;
    private FurqanSettings sSettings;
    private Boolean initialized = false;

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Initialize the singletons so their instances
        // are bound to the application process.
        sFurqanApp = this;

    }

    private void initSingletons()
    {
        sFurqanDatabase = new FurqanDatabase(sFurqanApp);
        sTranslationsRepository = new TranslationsRepository(sFurqanDatabase);
        sTranslationsService = new TranslationsService(sTranslationsRepository);
        sSettings = new FurqanSettings(this);
        initialized = true;
    }

    public static FurqanApp getInstance() {
        if(!sFurqanApp.initialized){
            sFurqanApp.initSingletons();
        }
        return sFurqanApp;
    }

    public static FurqanSettings getSettings() {
        return  getInstance().sSettings;
    }

    public static TranslationsService getTranslationsService() {
        return getInstance().sTranslationsService;
    }
}

