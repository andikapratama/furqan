package com.andikapratama.furqan;

import android.graphics.Typeface;
import android.preference.PreferenceManager;

import com.andikapratama.furqan.models.Translation;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * Created by andikapratama on 14/6/13.
 */
public class FurqanSettings {
    FurqanApp app;

    public FurqanSettings(FurqanApp app) {
        this.app = app;

        typefaceChoice = Maps.newHashMap();
        typefaceChoice.put("Scheherazade",Typeface.createFromAsset(app.getAssets(), "ScheherazadeRegOT.ttf"));
        typefaceChoice.put("Lateef", Typeface.createFromAsset(app.getAssets(), "LateefRegOT.ttf"));
    }

    private Optional<List<Translation>> selectedTranslations = Optional.absent();

    private Map<String,Typeface> typefaceChoice;

    public float getArabicTextSize() {
        return Float.valueOf(PreferenceManager.getDefaultSharedPreferences(app).getString("arabic_size", "36"));
    }

    public float getTextSize() {
        return Float.valueOf(PreferenceManager.getDefaultSharedPreferences(app).getString("text_size", "16"));
    }

    public  Typeface getArabicTypeface()
    {
        return typefaceChoice.get(PreferenceManager.getDefaultSharedPreferences(app).getString("arabic_typeface", "Scheherazade"));
    }

    public void refreshTranslations()
    {
        selectedTranslations = Optional.of(FurqanApp.getTranslationsService().getAvailableTranslations());
    }

    public synchronized List<Translation> getSelectedTranslations() {
        if (selectedTranslations.isPresent())
        {
            return selectedTranslations.get();
        }
        refreshTranslations();
        return this.getSelectedTranslations();
    }
}
