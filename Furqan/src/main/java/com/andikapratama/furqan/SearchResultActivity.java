package com.andikapratama.furqan;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.andikapratama.furqan.models.SearchResult;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by andikapratama on 1/10/13.
 */
public class SearchResultActivity extends ListActivity {
    List<SearchResult> results = Lists.newArrayList();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.sura_activity);
        handleIntent(getIntent());
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
        super.onListItemClick(l, view, position, id);
        SearchResult sr = results.get(position);
        Intent intent = new Intent(this, VerseActivity.class);
        intent.putExtra(VerseActivity.SURAH_NUMBER,sr.getSurahNo());
        intent.putExtra(VerseActivity.VERSE_NUMBER,sr.getNumber());
        startActivity(intent);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String query = intent.getStringExtra(SearchManager.QUERY);
            String[] emptyList = new String[] {"Searching.."};
            setListAdapter(new ArrayAdapter<String>(SearchResultActivity.this, android.R.layout.simple_list_item_1, emptyList));

            new AsyncTask<Void,Void,Void>(){
                @Override
                protected Void doInBackground(Void... voids) {
                    results = FurqanApp.getTranslationsService().searchTranslationsContaining(query);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    if (results.size()==0)
                    {
                        String[] emptyList = new String[] {"No record found"};
                        setListAdapter(new ArrayAdapter<String>(SearchResultActivity.this, android.R.layout.simple_list_item_1, emptyList));
                    }else {
                        setListAdapter(new SearchResultAdapter(SearchResultActivity.this,results,query));
                        getListView().setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    }
                    super.onPostExecute(aVoid);
                }
            }.execute();
            //use the query to search your data somehow
        }
    }


}
