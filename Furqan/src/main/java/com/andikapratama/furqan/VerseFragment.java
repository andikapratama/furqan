package com.andikapratama.furqan;

/**
 * Created by andikapratama on 17/9/13.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.andikapratama.furqan.models.Verse;

/**
 * A dummy fragment representing a section of the app, but that simply
 * displays dummy text.
 */
public class VerseFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static final String SURAH_NUMBER = "surah_number";
    public static final String VERSE_NUMBER = "verse_number";

    private int surahNo;
    private int verseNo;
    private Verse mverse;
    private ListView listView;
    private TextView arabicTextView;
    private VerseListAdapter adapter;


    SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(FurqanApp.getInstance());
    SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            adapter.notifyDataSetChanged();
        }
    };;

    @Override
    public void onResume() {
        super.onResume();
        preference.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        preference.unregisterOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_dummy, container, false);
        listView = (ListView)rootView.findViewById(R.id.listView);
        surahNo = getArguments().getInt(SURAH_NUMBER,1);
        verseNo = getArguments().getInt(VERSE_NUMBER,1);

        mverse = FurqanApp.getTranslationsService().getVerse(surahNo,verseNo,FurqanApp.getSettings().getSelectedTranslations());
        adapter = new VerseListAdapter(mverse,getActivity());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView verseText = (TextView) view.findViewById(R.id.verseText);
                TextView sourceText = (TextView) view.findViewById(R.id.sourceLabel);
                ImageView imageView = (ImageView) view.findViewById(R.id.imageCollapsible);
                if (verseText.getVisibility() == View.VISIBLE)
                {
                    verseText.setVisibility(View.GONE);
                    imageView.setImageResource(R.drawable.navigation_expand);
                    if (i==0)
                    {
                        View padding = view.findViewById(R.id.optional_arabic_padding);
                        sourceText.setText("Arabic Text");
                        padding.setVisibility(View.GONE);
                        imageView.setVisibility(View.VISIBLE);
                        sourceText.setVisibility(View.VISIBLE);
                    }
                }else
                {
                    verseText.setVisibility(View.VISIBLE);
                    imageView.setImageResource(R.drawable.navigation_collapse);

                    if (i==0)
                    {
                        View padding = view.findViewById(R.id.optional_arabic_padding);
                        sourceText.setText("");
                        padding.setVisibility(View.VISIBLE);
                        imageView.setVisibility(View.GONE);
                        sourceText.setVisibility(View.GONE);
                    }
                }
            }
        });

        return rootView;
    }

}
