/**
 * Created by andikapratama on 25/6/13.
 */

package com.andikapratama.furqan;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.andikapratama.furqan.models.Translation;
import com.andikapratama.furqan.models.Verse;
import com.google.common.base.Strings;

public class VerseListAdapter extends BaseAdapter
{
    static class ViewHolder {
        public TextView sourceText;
        public TextView dataText;
        public View padding;
    }

    Verse mVerse;
    Activity context;

    public VerseListAdapter(Verse mVerse, Activity context) {
        this.mVerse = mVerse;
        this.context = context;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public int getCount() {
        return mVerse.getTranslations().size()+1;
    }

    @Override
    public Translation getItem(int i) {
        return i == 0 ? null : FurqanApp.getSettings().getSelectedTranslations().get(i-1);
    }

    @Override
    public long getItemId(int i) {
        return i == 0 ? 0 : FurqanApp.getSettings().getSelectedTranslations().get(i-1).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {


        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.verse_row,null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.sourceText = (TextView) rowView.findViewById(R.id.sourceLabel);
            viewHolder.dataText = (TextView) rowView
                    .findViewById(R.id.verseText);
            viewHolder.dataText.setTextSize(FurqanApp.getSettings().getTextSize());
            viewHolder.padding = rowView.findViewById(R.id.optional_arabic_padding);
            rowView.setTag(viewHolder);

        }
        final ViewHolder holder = (ViewHolder) rowView.getTag();

        if(i==0)
        {
            holder.dataText.setTypeface(FurqanApp.getSettings().getArabicTypeface());
            holder.dataText.setTextSize(FurqanApp.getSettings().getArabicTextSize());
            holder.sourceText.setText("");

            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageCollapsible);
            imageView.setVisibility(View.GONE);
            holder.padding.setVisibility(View.VISIBLE);
            holder.sourceText.setVisibility(View.GONE);
            holder.dataText.setText(Html.fromHtml(mVerse.getArabicText()));

            holder.dataText.setTextSize(FurqanApp.getSettings().getArabicTextSize());
            holder.dataText.setTypeface(FurqanApp.getSettings().getArabicTypeface());
            return rowView;
        }
        Translation translation = getItem(i);
        String text = Strings.nullToEmpty(mVerse.getTranslations().get(translation));
        holder.dataText.setText(Html.fromHtml(text));
        holder.sourceText.setText(translation.getTranslator());
        holder.padding.setVisibility(View.GONE);

        return rowView;
    }
}

