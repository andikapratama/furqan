package com.andikapratama.furqan.services;

import com.google.common.base.Optional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andikapratama on 31/7/13.
 */
public class Utils {

    static final SimpleDateFormat dateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");

    static public Optional<Date> dateFromString(String str)
    {
        Date date = null;
        try {
            date = dateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Optional.of(date);
    }
}
