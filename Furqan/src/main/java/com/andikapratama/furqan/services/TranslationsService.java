package com.andikapratama.furqan.services;

import com.andikapratama.furqan.models.SearchResult;
import com.andikapratama.furqan.models.Source;
import com.andikapratama.furqan.models.Surah;
import com.andikapratama.furqan.models.Translation;
import com.andikapratama.furqan.models.TranslationData;
import com.andikapratama.furqan.models.Verse;
import com.andikapratama.furqan.repository.TranslationsRepository;
import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * Created by andikapratama on 13/6/13.
 */
public class TranslationsService {

    private final TranslationsRepository mTranslationsRepository;

    public synchronized List<SearchResult> searchTranslationsContaining(String query)
    {
        return mTranslationsRepository.searchTranslationsContaining(query);
    }

    public TranslationsService(TranslationsRepository translationsRepository) {
        mTranslationsRepository = translationsRepository;
    }

    public void moveSourcesOrder(int idfrom, int from, int to)
    {
        mTranslationsRepository.moveSourcesOrder(idfrom,from,to);
    }

    public synchronized void updateSourceStatus(int id,String status)
    {
        mTranslationsRepository.updateSourceStatus(id,status);
    }

    public synchronized void deleteSourcesData(int id)
    {

        try {
            mTranslationsRepository.mDatabase.beginTransaction();
            mTranslationsRepository.deleteTranslationData(id);
            mTranslationsRepository.updateSourceStatus(id,"notdownloaded");
            mTranslationsRepository.mDatabase.setTransactionSuccessful();
        }finally {
            mTranslationsRepository.mDatabase.endTransaction();
        }
    }

    public boolean saveTanzilTranslationResourceAndEnableIt(int id, Reader readInput) {
        BufferedReader br = new BufferedReader(readInput);

        try {
            String line = br.readLine();
            while (line != null) {
                //System.out.println(read);
                String[] value = Iterables.toArray(Splitter.on('|').trimResults().split(line), String.class);
                String SuraNo = value[0];
                String verseNo = value[1];
                String translation = value[2];
                mTranslationsRepository.insertTranslationDataRecord(id, Integer.valueOf(verseNo), Integer.valueOf(SuraNo), translation);
                line = br.readLine();
            }
            mTranslationsRepository.updateSourceStatus(id, "enabled");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveTanzilTranslationResourceAndEnableIt(int id, File file) {
        long startTime = System.currentTimeMillis();


        boolean result = true;
        List<String> lines = Lists.newArrayList();
        try {
            lines = FileUtils.readLines(file);
        } catch (IOException e) {
            e.printStackTrace();
            result = false;
        }

        List<TranslationData> datas = Lists.newArrayListWithCapacity(lines.size());

        for (String line : lines) {
            String[] value = Iterables.toArray(Splitter.on('|').trimResults().split(line), String.class);
            if (value.length < 3)
                continue;
            String SuraNo = value[0];
            String verseNo = value[1];
            String translation = value[2];
            datas.add(new TranslationData(id,Integer.valueOf(verseNo), Integer.valueOf(SuraNo), translation));
        }

        mTranslationsRepository.insertTranslationDataRecordBulk(datas);

        mTranslationsRepository.updateSourceStatus(id, "enabled");
        long endTime = System.currentTimeMillis();

        System.out.println("That took " + (endTime - startTime) + " milliseconds");

        return result;
    }

    public List<Translation> getAvailableTranslations() {
        return mTranslationsRepository.getAvailableTranslations();
    }

    public List<Source> getAvailableSources() {
        return mTranslationsRepository.getAvailableSources();
    }

    public Verse getVerse(int surahNo, int verseNo, Iterable<Translation> translations) {
        return mTranslationsRepository.getVerse(surahNo, verseNo, translations);
    }

    List<Surah> surahsCache;

    public List<Surah> getAllSurah() {
        if (surahsCache==null)
        {
            surahsCache = mTranslationsRepository.getAllSurah();
        }
        return surahsCache;
    }

    public Optional<Surah> getSurah(int surahNo) {
        return Optional.of(getAllSurah().get(surahNo-1));
    }
}
