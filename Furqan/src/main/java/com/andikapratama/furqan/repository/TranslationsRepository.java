package com.andikapratama.furqan.repository;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.andikapratama.furqan.models.SearchResult;
import com.andikapratama.furqan.models.Source;
import com.andikapratama.furqan.models.Surah;
import com.andikapratama.furqan.models.Translation;
import com.andikapratama.furqan.models.TranslationData;
import com.andikapratama.furqan.models.Verse;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import java.sql.Date;
import java.util.List;

/**
 * Created by andikapratama on 11/6/13.
 */
public class TranslationsRepository {

    final SQLiteOpenHelper mSQLite;
    public final SQLiteDatabase mDatabase;

    public TranslationsRepository(SQLiteOpenHelper sqlHelper) {
        mSQLite = sqlHelper;
        mDatabase = mSQLite.getWritableDatabase();
    }

    public synchronized List<SearchResult> searchTranslationsContaining(String query)
    {

        List<SearchResult> results = Lists.newArrayList();
        Cursor cursor = mDatabase.rawQuery("SELECT td.SuraId,td.VerseId,td.VerseText,s.Name FROM TranslationData td LEFT JOIN Sources s on td.TranslationId = s.Id WHERE s.Status = 'enabled' AND td.VerseText LIKE '%' || ? || '%'",new String[]{query});

        int suraId = cursor.getColumnIndex("SuraId");
        int verseId = cursor.getColumnIndex("VerseId");
        int verseText = cursor.getColumnIndex("VerseText");
        int name = cursor.getColumnIndex("Name");

        while (cursor.moveToNext())
        {
            results.add(new SearchResult(
                    cursor.getString(verseText),
                    cursor.getInt(verseId),
                    cursor.getInt(suraId),
                    cursor.getString(name)
            ));
        }
        cursor.close();

        return results;
    }

    public synchronized List<Source> getAvailableSources()
    {
        List<Source> sources = Lists.newArrayList();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM Sources ORDER BY Ordering",null);

        int id = cursor.getColumnIndex("Id");
        int type = cursor.getColumnIndex("Type");
        int name = cursor.getColumnIndex("Name");
        int author = cursor.getColumnIndex("Author");
        int downloadLink = cursor.getColumnIndex("DownloadLink");
        int updateLink = cursor.getColumnIndex("UpdateLink");
        int lastModifiedDate = cursor.getColumnIndex("LastModifiedDate");
        int language = cursor.getColumnIndex("Language");
        int providerName = cursor.getColumnIndex("ProviderName");
        int status = cursor.getColumnIndex("Status");

        while (cursor.moveToNext())
        {
            Source source = new Source(
                    cursor.getInt(id),
                    cursor.getString(type),
                    cursor.getString(name),
                    cursor.getString(author),
                    cursor.getString(downloadLink),
                    cursor.getString(updateLink),
                    Date.valueOf(cursor.getString(lastModifiedDate)),
                    cursor.getString(language),
                    cursor.getString(providerName),
                    cursor.getString(status)
            );
            sources.add(source);
        }
        cursor.close();
        return sources;
    }

    public void moveSourcesOrder(int idfrom, int from, int to)
    {
        if (from == to)
            return;

        if (from < to)
        {
            int move = -1;

            mDatabase.execSQL("UPDATE Sources SET Ordering = (Ordering + (?)) WHERE Ordering > ? AND Ordering <= ?"
                    ,new String[]{
                    String.valueOf(move),
                    String.valueOf(from),
                    String.valueOf(to)});
        }
        else {
            int move = 1;

            mDatabase.execSQL("UPDATE Sources SET Ordering = (Ordering + (?)) WHERE Ordering >= ? AND Ordering < ?"
                    ,new String[]{
                    String.valueOf(move),
                    String.valueOf(to),
                    String.valueOf(from)});
        }

        mDatabase.execSQL("UPDATE Sources SET Ordering = ? WHERE Id = ?"
                ,new String[]{
                String.valueOf(to),
                String.valueOf(idfrom)});
    }


    public synchronized void deleteTranslationData(int id)
    {
            mDatabase.execSQL("DELETE FROM TranslationData where TranslationId = ?"
                    ,new String[]{
                    String.valueOf(id)});
    }

    public synchronized void updateSourceStatus(int id,String status)
    {
        mDatabase.execSQL("UPDATE Sources SET Status = ? where Id = ?"
                ,new String[]{
                status,
                String.valueOf(id)});
    }

    public synchronized void insertTranslationDataRecordBulk(Iterable<TranslationData> datas)
    {
        final SQLiteStatement statement = mDatabase.compileStatement("INSERT INTO TranslationData(TranslationId,SuraId,VerseId, VerseText) VALUES(?,?,?,?)");
        try {
            mDatabase.beginTransaction();

            for (TranslationData data : datas)
            {
                statement.clearBindings();
                statement.bindLong(1,data.id);
                statement.bindLong(2,data.surahNo);
                statement.bindLong(3,data.verseNo);
                statement.bindString(4,data.translation);
                statement.executeInsert();
            }

            mDatabase.setTransactionSuccessful();
        }
        catch (Exception e)
        {
        }
        finally {
            mDatabase.endTransaction();
        }
    }

    public synchronized void insertTranslationDataRecord(int id,int verseNo,int surahNo,
            String translation)
    {
        mDatabase.execSQL("INSERT INTO TranslationData(TranslationId,SuraId,VerseId, VerseText) VALUES(?,?,?,?)"
                ,new String[]{
                String.valueOf(id),
                String.valueOf(surahNo),
                String.valueOf(verseNo),
                translation});
    }

    public List<Translation> getAvailableTranslations()
    {
        List<Translation> translations = Lists.newArrayList();
        Cursor cursor = mDatabase.rawQuery("SELECT Id,Name,Language,LastModifiedDate,Ordering FROM Sources WHERE Type = 'Translation' AND Status = 'enabled' ORDER BY Ordering",null);
        while (cursor.moveToNext())
        {
            Translation translation = new Translation(
                    cursor.getInt(cursor.getColumnIndex("Id")),
                    cursor.getString(cursor.getColumnIndex("Name")),
                    cursor.getString(cursor.getColumnIndex("Language")),
                    Date.valueOf(cursor.getString(cursor.getColumnIndex("LastModifiedDate"))),
                    cursor.getInt(cursor.getColumnIndex("Ordering"))
            );
            translations.add(translation);
        }
        cursor.close();
        return  translations;
    }

    public void addTranslationToVerse(Verse verse,Translation translation)
    {
        Cursor cursor = mDatabase.rawQuery("SELECT VerseText FROM TranslationData WHERE TranslationId = ? AND VerseId = ? AND SuraID = ? ",
                new String[]{
                        String.valueOf(translation.getId()),
                        String.valueOf(verse.getNumber()),
                        String.valueOf(verse.getSurahNo())
                }
        );

        if (cursor.moveToNext())
        {
            verse.getTranslations().put(translation,cursor.getString(0));
        }

        cursor.close();
    }

    public String getArabicText(int surahNo,int verseNo)
    {
        Cursor cursor = mDatabase.rawQuery("SELECT VerseText FROM ArabicText WHERE VerseId = ? AND SuraId = ? ",
                new String[]{
                        String.valueOf(verseNo),
                        String.valueOf(surahNo)
                }
        );

        String arabicText = "";

        if (cursor.moveToNext())
        {
            arabicText = cursor.getString(0);
        }

        cursor.close();
        return arabicText;
    }

    public Verse getVerse(int surahNo,int verseNo,Iterable<Translation> translations)
    {
        Verse verse = new Verse(surahNo,verseNo,getArabicText(surahNo, verseNo));

        for (Translation translation : translations)
        {
            this.addTranslationToVerse(verse,translation);
        }

        return verse;
    }

    public Optional<Surah> getSurah(int surahNo)
    {
        Surah surah = null;
        Cursor cursor = mDatabase.rawQuery("SELECT Id,TransliterationName,TranslationName,VerseCount,ArabicName,Type FROM Surah WHERE Id = ?",new String[]{String.valueOf(surahNo)});
        if (cursor.moveToNext())
        {
            surah = new Surah(surahNo,
                    cursor.getString(cursor.getColumnIndex("TransliterationName")),
                    cursor.getString(cursor.getColumnIndex("TranslationName")),
                    cursor.getString(cursor.getColumnIndex("ArabicName")),
                    cursor.getString(cursor.getColumnIndex("Type")),
                    cursor.getInt(cursor.getColumnIndex("VerseCount"))
            );
        }
        cursor.close();
        return Optional.of(surah);
    }

    public List<Surah> getAllSurah()
    {
        List<Surah> surahs = Lists.newArrayList();
        Cursor cursor = mDatabase.rawQuery("SELECT Id,TransliterationName,TranslationName,VerseCount,ArabicName,Type FROM Surah ORDER BY Id ASC",new String[]{});
        while (cursor.moveToNext())
        {
            surahs.add(new Surah(cursor.getInt(cursor.getColumnIndex("Id")),
                    cursor.getString(cursor.getColumnIndex("TransliterationName")),
                    cursor.getString(cursor.getColumnIndex("TranslationName")),
                    cursor.getString(cursor.getColumnIndex("ArabicName")),
                    cursor.getString(cursor.getColumnIndex("Type")),
                    cursor.getInt(cursor.getColumnIndex("VerseCount"))
            ));
        }
        cursor.close();
        return surahs;
    }
}
