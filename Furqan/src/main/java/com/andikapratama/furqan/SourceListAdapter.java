package com.andikapratama.furqan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.andikapratama.furqan.models.Source;
import com.andikapratama.furqan.views.dragsortlistview.DragSortListView;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by andikapratama on 31/7/13.
 */
public class SourceListAdapter extends BaseAdapter implements DragSortListView.DropListener {

    Context context;
    private int mDivPos;

    List<Source> downloadedSources;
    List<Source> availableSources;
    DragSortListView listView;

    public SourceListAdapter(final Context context,final List<Source> objects) {
        this.context = context;
        this.listView = listView;
        downloadedSources = Lists.newArrayList();
        availableSources = Lists.newArrayList();
        for (Source source:objects)
        {
            if (source.getStatus().equals("notdownloaded"))
            {
                availableSources.add(source);
            }else
            {
                downloadedSources.add(source);
            }
        }
        mDivPos = downloadedSources.size()+1;
    }

    @Override
    public int getCount() {
        return downloadedSources.size() + availableSources.size() + 2;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (i == 0) {
            return "Downloaded";
        }
        if (i == mDivPos) {
            return "Available";
        }
        if (i < mDivPos)
        {
            return downloadedSources.get(i-1);
        }
        if (i > mDivPos)
        {
            return availableSources.get(i-(1+mDivPos));
        }
        return null;
    }

    @Override
    public boolean isEnabled(int position) {
        return position != mDivPos && position != 0;
    }

    public int getDivPosition() {
        return mDivPos;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position == mDivPos) {
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public void drop(int from, int to) {
        if (from != to) {
            from -= 1;
            to -= 1;
            //DragSortListView list = getListView();
            Source item = downloadedSources.get(from);
            FurqanApp.getTranslationsService().moveSourcesOrder(item.id,from,to);
            downloadedSources.remove(item);
            downloadedSources.add(to,item);

            notifyDataSetChanged();
            //list.moveCheckState(from, to);

        }

    }

    static class ViewHolder {
        public TextView langLabel;
        public TextView sourceLabel;
        public ToggleButton statusToggle;
        public ImageButton deleteButton;
        public View drag_holder;
        public Source source;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Object item = getItem(position);
        if (item instanceof String) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View sectionView = inflater.inflate(R.layout.sources_section_div, null);
            TextView sectionTitle = (TextView)sectionView.findViewById(R.id.sectionTextView);
            sectionTitle.setText((String)item);
            return sectionView;
        } else {
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.source_row, null);
                final ViewHolder viewHolder = new ViewHolder();

                viewHolder.langLabel = (TextView) rowView.findViewById(R.id.languageLabel);
                viewHolder.sourceLabel = (TextView) rowView.findViewById(R.id.sourceLabel);
                viewHolder.statusToggle = (ToggleButton) rowView.findViewById(R.id.toggleButton);
                viewHolder.deleteButton = (ImageButton)rowView.findViewById(R.id.imageButton);
                viewHolder.drag_holder = (View)rowView.findViewById(R.id.drag_handle);
                viewHolder.statusToggle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewHolder.source.setStatus(viewHolder.statusToggle.isChecked() ? "enabled" : "disabled");
                        FurqanApp.getTranslationsService()
                                .updateSourceStatus(viewHolder.source.id,
                                        viewHolder.source.getStatus());
                    }
                });

                viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(context)
                                .setTitle("Confirm Delete")
                                .setMessage("Delete the source data? you can always redownload it later.")
                                .setNegativeButton("Cancel", null)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        FurqanApp.getTranslationsService().deleteSourcesData(viewHolder.source.id);
                                    }
                                })
                                .show();
                    }
                });

                rowView.setTag(viewHolder);
            }
            ViewHolder holder = (ViewHolder) rowView.getTag();
            Source source = (Source) item;
            holder.source = source;
            holder.langLabel.setText(source.language);
            holder.sourceLabel.setText(Html.fromHtml(source.name));
            holder.statusToggle.setChecked(source.status.equals("enabled"));

            if (!source.getStatus().equals("notdownloaded")) {
                holder.statusToggle.setVisibility(View.VISIBLE);
                holder.deleteButton.setVisibility(View.VISIBLE);
                holder.drag_holder.setVisibility(View.VISIBLE);
            } else {
                holder.statusToggle.setVisibility(View.GONE);
                holder.deleteButton.setVisibility(View.GONE);
                holder.drag_holder.setVisibility(View.GONE);
            }
            return rowView;
        }
    }
}
