package com.andikapratama.furqan;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andikapratama.furqan.models.Surah;

import java.util.List;

/**
 * Created by andikapratama on 30/6/13.
 */
public class SurahListAdapter extends BaseAdapter {

    List<Surah> surahs;
    Activity context;

    public SurahListAdapter(Activity context)
    {
        this.context = context;
        surahs = FurqanApp.getInstance().getTranslationsService().getAllSurah();
    }

    @Override
    public int getCount() {
        return 114;
    }

    @Override
    public Surah getItem(int i) {
        return surahs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getNo();
    }

    static class ViewHolder {
        public TextView arabicTitle;
        public TextView transliterationTitle;
        public TextView translationTitle;
        public TextView additionalInfo;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.sura_row,null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.arabicTitle = (TextView) rowView.findViewById(R.id.arabicTitle);
            viewHolder.translationTitle = (TextView) rowView.findViewById(R.id.translationTitle);
            viewHolder.additionalInfo = (TextView) rowView.findViewById(R.id.additionalInfo);
            viewHolder.transliterationTitle = (TextView) rowView.findViewById(R.id.latinTitle);

            viewHolder.arabicTitle.setTypeface(FurqanApp.getSettings().getArabicTypeface());
            viewHolder.arabicTitle.setTextSize(FurqanApp.getSettings().getArabicTextSize());
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Surah surah = getItem(i);
        holder.additionalInfo.setText(surah.getVerseCount() +" verses, "+surah.getType());
        holder.transliterationTitle.setText(surah.getNo()+  ". " + Html.fromHtml(surah.getName()));
        holder.translationTitle.setText(surah.getTranslationName());
        holder.arabicTitle.setText(surah.getArabicName());

        return rowView;
    }
}
